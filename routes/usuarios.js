const express = require('express');
const Joi = require('joi');
const router = express.Router();



const usuarios = [
    {id:1, nombre:'katy'},
    {id:2, nombre:'fernanda'},
    {id:3, nombre:'jorge'}
];

router.get('/',(req, res) => {
    res.send(usuarios);
});
router.get('/:id', (req, res) => {
    let usuario = existeUsuario(req.params.id);
    usuario ? res.send(usuario) : res.status(404).send('Usuario no encontrado');
});

router.post('/', (req, res) => {
    const name = req.body.nombre;
    const {error, value} = validarUsuario(name);
    if(!error){
        const usuario = {
            id: usuarios.length + 1,
            nombre: value.nombre
        };
        usuarios.push(usuario);
        res.send(usuario);
    }else{
        res.status(400).send(`${error}`);
    }
});

router.put('/:id', (req, res) => {
    let usuario = existeUsuario(req.params.id);
    if (!usuario) {
        res.status(404).send('Usuario no encontrado');
        return;
    }
    const name = req.body.nombre;
    const {error, value} = validarUsuario(name);
    if(error){
        res.status(400).send(`${error}`)   
    }else{
        usuario.nombre = value.nombre;
        res.status(200).send(usuario);
    }
});
router.delete('/:id', (req, res) => {
    let usuario = existeUsuario(req.params.id);
    if(!usuario){
        res.status(404).send('Usuario no encontrado');
        return;
    }else{
        const index = usuarios.indexOf(usuario);
        usuarios.splice(index,1);
        res.status(200).send('Usuario eliminado');
    }
});
const existeUsuario = (id) => {
    return usuarios.find(user => user.id === parseInt(id))
};
const validarUsuario = (name) => {
    const schema = Joi.object({
        nombre: Joi.string().min(3).required()
    });
    return schema.validate({nombre: name});
};

module.exports = router;