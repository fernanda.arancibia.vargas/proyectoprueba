const debug = require('debug')('app:inicio');
//const dbDebug = require('debug')('app:db');
const express = require('express');
const config = require('config');
// const logger = require('./logger');
const morgan = require('morgan');
const app = express();
const usuarios = require('./routes/usuarios')
const enviroment = app.get('env'); //cambiar entorno export NODE_ENV=development/production


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(express.static('public'));
app.use('/api/usuarios', usuarios)

//Configuración de entornos
console.log('Aplicación: ' + config.get('nombre'));
console.log('BD server: ' + config.get('configDB.host'))

//Uso de middleware de tercero - Morgan
if(enviroment === 'development'){
    app.use(morgan('tiny')); // mostrar los logs de las peticiones http
    debug('Morgan está habilitado');
};
// trabajos con bd
debug('Conectando con la db') //para habilitar en consola export DEBUG=app:db, habilitar todas DEBUG= app:*

// app.use(logger);


app.get('/', (req,res) => {
    res.send('Hola desde express');
});


const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`escuchando en puerto ${port}`);
});


